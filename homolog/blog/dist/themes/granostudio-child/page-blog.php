<?php
/**
* Page Template
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

get_header();?>
<div class="blog blog-home" >
	<span class='scroll-debug' scroll-position="scroll">{{scroll}}</span>
	<!-- Banner -->
	<div class="blog-banner" style="background-image:url(http://via.placeholder.com/400x300)">
	  <div class="content">
	    <div class="cat">
	      <a href="#">Destaque</a>, <a href="#">categoria A</a>, <a href="#">categoria B</a>
	    </div>
	    <h1>Loren ipsum dolor sit amet adispicing consectectur</h1>
	    <div class="btn-lg btn-spread icon-seta">
	      Leia na íntegra
	      <span></span>
	    </div>
	  </div>
	    <div class="mask">
	      <svg
	     version="1.1"
	     id="no-aspect-ratio"
	     xmlns="http://www.w3.org/2000/svg"
	     xmlns:xlink="http://www.w3.org/1999/xlink"
	     x="0px"
	     y="0px"
	     height="100%"
	     width="100%"
	     viewBox="0 0 100 100"
	     preserveAspectRatio="none">
	        <polygon fill="white" points="100,100 0,100 0,0 "/>
	      </svg>
	    </div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="nav-cat">
					<div class="dropdown hidden-md-up">
					  <a class="btn dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    Categorias
					  </a>

					  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					    <a class="dropdown-item" href="#">Action</a>
					    <a class="dropdown-item" href="#">Another action</a>
					    <a class="dropdown-item" href="#">Something else here</a>
					  </div>
					</div>
					<div class="lista hidden-md-down">
						<a href="#">Categoria</a>
						<a href="#">Categoria</a>
						<a href="#">Categoria</a>
						<a href="#">Categoria</a>
					</div>
				</div>
				<div class="btn-lg btn-spread icon-lupa busca-principal">
					Buscar no blog
					<span></span>
				</div>
			</div>
		</div>
		<div class="row loop">
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-md destaque ">
						<div class="post height-2" style="background-image:url(http://via.placeholder.com/400x300)">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col destaque-2">
						<div class="post">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
						<div class="post">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 loop-2">
				<div class="row">
					<div class="col-12 col-md-6 col-lg-4">
						<div class="post" style="background-image:url(http://via.placeholder.com/400x300)">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-4">
						<div class="post" style="background-image:url(http://via.placeholder.com/400x300)">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-4">
						<div class="post" style="background-image:url(http://via.placeholder.com/400x300)">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-4">
						<div class="post" style="background-image:url(http://via.placeholder.com/400x300)">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-4">
						<div class="post" style="background-image:url(http://via.placeholder.com/400x300)">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-4">
						<div class="post" style="background-image:url(http://via.placeholder.com/400x300)">
							<div class="content">
								<div class="cat">
									Business Solution
								</div>
								<h2>
									Loren ipsum dolor sit amet adispicing
								</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent convallis dapibus lacus sed congue. Nulla vitae blandit ipsum. Sed lacinia libero ut nibh convallis, eget accumsan ligula accumsan.</p>
								<div class="footer">
									<div class="redes-sociais">
										<a class="face icon icon-social-facebook">Facebook</a>
										<a class="twitter icon icon-social-twitter">Twitter</a>
										<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
									</div>
									<div class="btn-lg btn-spread icon-seta">
										saiba mais
										<span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<nav aria-label="..." class="paginacao">
	  <ul class="pagination justify-content-center">
	    <li class="page-item">
	      <a class="page-link icon-seta" href="#" aria-label="Previous">
	        <span class="sr-only">Previous</span>
	      </a>
	    </li>
	    <li class="page-item"><a class="page-link" href="#">1</a></li>
	    <li class="page-item"><a class="page-link" href="#">2</a></li>
	    <li class="page-item"><a class="page-link" href="#">3</a></li>
	    <li class="page-item">
	      <a class="page-link icon-seta" href="#" aria-label="Next">
	        <span class="sr-only">Next</span>
	      </a>
	    </li>
	  </ul>
	</nav>

</div>


<?php

// page

while ( have_posts() ) : the_post();



	// End of the loop.
endwhile;

 ?>

<?php get_footer(); ?>
