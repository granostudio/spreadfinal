<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */


/**
 * Enqueues scripts and styles.
 * teste 1
 */

function granostudio_scripts_child() {

	//Desabilitar jquery
	wp_deregister_script( 'jquery' );

	// Theme stylesheet.
	wp_enqueue_style( 'granostudio-style', get_stylesheet()  );
	// import fonts (Google Fonts)
	wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
	// Theme front-end stylesheet
	wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.css');


	// scripts js
	wp_enqueue_script('granostudio-jquery', get_stylesheet_directory_uri() . '/js/jquery-3.1.1.js', '000311', false);
	wp_enqueue_script('granostudio-angular', get_stylesheet_directory_uri() . '/js/angular.min.js', '000311', false);
	wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/scripts.min.js', '000001', false, true);
	// helper angular
	wp_localize_script( 'granostudio-scripts', 'aeJS',
		array(
			'themeUrl' => get_stylesheet_directory_uri(),
		)
	);


}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
