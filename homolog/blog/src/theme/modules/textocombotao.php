<?php

/**
* Módulo:
* ***** Texto com botao- Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_textocombotao($textocombotao_titulo, $textocombotao_conteudo, $textocombotao_link_titulo,$textocombotao_link_url, $key)
{
  ?>
    <div id="textocombotao" class="container textocombotao-<?php echo $key;?> ">
      <div class="row">

          <div class="col-sm-12 titulo">
            <h2><?php echo $textocombotao_titulo; ?></h2>
          </div>
          <div class="col-sm-12 texto">
            <?php echo $textocombotao_conteudo; ?>
          </div>
          <?php if(!empty($textocombotao_link_titulo)){
            ?>
            <div class="col-sm-12 botao">
              <a href="<?php echo $textocombotao_link_url;?>" class="btn btn-primary"><?php echo $textocombotao_link_titulo;?></a>
            </div>
            <?php
            }
           ?>
      </div>
    </div>
<?php
}
 ?>
