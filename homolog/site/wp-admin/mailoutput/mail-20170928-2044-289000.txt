To: contato@ideiaseguros.com.br
Subject: Novo site WordPress
Date: Thu, 28 Sep 2017 20:44:48 +0000
From: WordPress <wordpress@localhost>
Message-ID: <22cebcefdb78886cbf7f685c178ded34@localhost>
X-Mailer: PHPMailer 5.2.22 (https://github.com/PHPMailer/PHPMailer)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Seu novo site WordPress foi configurado com sucesso em:

http://localhost/ideia-seguros

Você pode fazer login como administrador usando as seguintes informações:

Usuário: admin_ideia
Senha: A senha que você escolheu durante a instalação.
Faça login aqui: http://localhost/ideia-seguros/wp-login.php

Esperamos que você goste do seu novo site. Obrigado!

--A equipe do WordPress
https://wordpress.org/

