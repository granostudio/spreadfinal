<?php
/**
 * Template Name: Duas colunas
 *
 * @package MS
 */

get_header();
$linhas = get_field('linhas');
$total_linhas = count( $linhas );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header" <?php fundo( umDe( get_post_thumbnail_id( $post->ID ), FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container ancorar">
				<div class="row">
					<div class="col-xs-12 col-sm-6 nao_ancorar anim" <?php animAttr( 'fadeIn', 1.4, 0.4 ) ?>>
						<?php
						elementoNaoVazio( 'h1', get_the_title(), cl('entry-title') );
						?>
					</div><!-- .col -->
					<div class="hidden-xs col-sm-6"></div>
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .header-caption -->
	</header><!-- .entry-header -->
	
	<div class="container">
		<div class="entry-main">
			<div class="entry-content">

				<?php for ( $i = 0; $i < $total_linhas; $i++ ) : ?>
					<div class="row duas_colunas">
						<div class="col-sm-12 col-md-6 coluna">
							<div class="anim" <?php animAttr( 'fadeInUp', 0.8, 1.3 ) ?>>
								<?php print $linhas[$i]['coluna1']; ?>
							</div><!-- .anim -->
						</div>
						<div class="clearfix visible-xs-block visible-sm-block"></div>
						<div class="col-sm-12 col-md-6 coluna">
							<div class="anim" <?php animAttr( 'fadeInUp', 0.8, 2 ) ?>>
								<?php print $linhas[$i]['coluna2']; ?>
							</div><!-- .anim -->
						</div>
					</div><!-- .row .duas_colunas -->
				<?php endfor; ?>
				
			</div><!-- .entry-content -->
		</div><!-- .entry-main -->
	</div><!-- .container -->
	
</article><!-- #post-## -->

<?php

get_footer();

