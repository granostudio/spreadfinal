<?php
get_header();
?>

<section id="arquivo">

	<header class="arquivo_cabecalho <?php if ( get_field( 'escurecer_fundo', PAGINA_BLOG ) ) print 'escurecer' ?>" <?php fundo( umDe( get_post_thumbnail_id( PAGINA_BLOG ), FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container ancorar">
				<?php
				if ( is_home() )
					$titulo = get_the_title( PAGINA_BLOG );
				elseif ( is_search() )
					$titulo = 'Busca por: &ldquo;' . esc_html( get_search_query() ) . '&rdquo;';
				else
					$titulo = get_the_archive_title();
				elementoNaoVazio( 'h1',
					subElemento( 'span', $titulo, cl('large') ).
					subElementoNaoVazio( 'small', enfase( get_field( 'subtitulo', PAGINA_BLOG ) ), cl('small') ),
				cl('arquivo_titulo') );
				?>
			</div><!-- .container -->
		</div><!-- .header-caption -->
	</header><!-- .arquivo_cabecalho -->
	
	<div class="container">
		<div class="entry-main">
			<div class="entry-content">
			
				<?php	
				
				Componente::renderizar( 'posts-blog', array(
				) );
				
				Componente::renderizar( 'paginacao' );

				?>
				
			</div><!-- .entry-content -->
		</div><!-- .entry-main -->
	</div><!-- .container -->
	

</section><!-- #arquivo -->

<?php
get_footer();