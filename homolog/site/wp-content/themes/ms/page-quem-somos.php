<?php
/**
 * Template Name: Quem Somos
 */
 
global $post;
 
add_action( 'wp_enqueue_scripts', 'telas_enqueue' );

get_header();
extract( get_fields() );

?>

	<?php /*
	<header class="entry-header" <?php fundo( umDe( $fundo, FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-7 anim" <?php animAttr( 'fadeInBottom', 1.4, 0.4 ) ?>>
						<?=
						E::h1( null, 'entry-title' )
							->span( $titulo1, 'linha1' )
							->span( $titulo2, 'linha2' )
							->span( $titulo3, 'linha3' )
						?>
					</div><!-- .col -->
					<div class="col-xs-12 col-sm-5 col-md-4 anim" <?php animAttr( 'fadeInLeft', 0.4, 2 ) ?>>
						<?=
						E::p( null, 'entry-desc' )
							->span( $desc, 'entry-desc-texto' )
						?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .header-caption -->
	</header>
	*/ ?>

<div id="quem-somos">
	
	<?php
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// CABEÇALHO
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	extract( $cabecalho[0], EXTR_PREFIX_ALL, 'cab' );
	
	?>
	
	<header class="quem-somos-banner" <?php fundo( $cab_fundo, 'full' ) ?>>
		<div class="content">
			<div class="container">
				
				<?=
				E::h1( null, 'cabecalho_titulo' )
					->span( $cab_titulo1, 'linha1' )
					->span( $cab_titulo2, 'linha2' )
				?>
				
				<div class="stats">
					<div class="row margem-40">
						<?php
						$classeCols = 'col-12 col-sm-3';
						$n = 0;
						foreach ( $cab_stats as $item ) :
							$n++;
							?>
							<div class="<?= $classeCols ?>">
								<div class="stats_item">
									<?=
									E::h2( $item['numero'] . '+', 'item_numero' ) .
									E::p( $item['rotulo'], 'item_rotulo' )
									?>
								</div><!-- .stat -->
							</div><!-- .col -->
							<?php
							row( $n, $classeCols );
						endforeach;
						?>
					</div><!-- .row -->
				</div><!-- .stats -->
				
			</div><!-- .container -->
		</div><!-- .content -->
		
		<?php
		ancora(
			'#clientes',
			imgTema( 'seta_turquesa.png', '&darr;', 'home_scroll_img' ),
			false,
			'home_scroll turquesa'
		);
		?>
	  
	    <div class="mask">
	      <svg
	     version="1.1"
	     id="no-aspect-ratio"
	     xmlns="http://www.w3.org/2000/svg"
	     xmlns:xlink="http://www.w3.org/1999/xlink"
	     x="0px"
	     y="0px"
	     height="100%"
	     width="100%"
	     viewBox="0 0 100 100"
	     preserveAspectRatio="none">
	        <polygon fill="white" points="100,100 0,100 0,0 "/>
	      </svg>
	    </div><!-- .mask -->
		
	</header><!-- .quem-somos-banner -->
	
	<?php
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// CLIENTES
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	extract( $clientes[0], EXTR_PREFIX_ALL, 'cli' );
	
	?>

	<section id="clientes" class="secao container">
	
		<?=
		E::h2( null, 'titulo clientes_titulo' )
			->span( $cli_titulo1, 'linha1' )
			->span( $cli_titulo2, 'linha2' )
		?>
		
		<div class="row thumb-clientes margem-30">
		  <div class="col-6 col-sm-4 col-md-2 col-lg-2">
			<img src="http://via.placeholder.com/150x150" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-4 col-md-2 col-lg-2">
			<img src="http://via.placeholder.com/150x150" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-4 col-md-2 col-lg-2">
			<img src="http://via.placeholder.com/150x150" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-4 col-md-2 col-lg-2">
			<img src="http://via.placeholder.com/150x150" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-4 col-md-2 col-lg-2">
			<img src="http://via.placeholder.com/150x150" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-4 col-md-2 col-lg-2">
			<img src="http://via.placeholder.com/150x150" alt="..." class="img-fluid img-thumbnail">
		  </div>
		</div><!-- .row -->
	
	</section><!-- #clientes -->
	
	<?php
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// EXECUTIVOS
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	extract( $executivos[0], EXTR_PREFIX_ALL, 'exec' );
	
	?>

	<section id="executivos" class="container">
	
		<div class="row margem-80">
		
			<div class="col-12 col-md-6 ajuste-padding">
				<?=
				E::h2( null, 'titulo executivos_titulo' )
					->span( $exec_titulo1, 'linha1' )
					->span( $exec_titulo2, 'linha2' )
				?>
				<div class="divisoria"></div>
				<?=
				E::p( $exec_texto, 'texto executivos_texto' )
				?>
			</div><!-- .col -->
		  
			<div class="col-12 col-sm-12 col-md-6 col-lg-6 img-executivos">
				<?php img( $exec_foto_lateral, 'full', cl('foto_lateral') ) ?>
			</div>
		  
		</div><!-- .row -->
		
		<div class="executivos_individuos vert_ancestral">
			<div class="row">
				<?php
				$classeCols = 'col-xs-6 col-md-4';
				$n = 0;
				foreach ( $exec_individuos as $item ) :
					$n++;
					?>
					<div class="<?= $classeCols ?>">
						<section class="individuos_item">
							<div class="item_imagem vert_container vert_item">
								<?=
								obterImg( $item['foto_estatica'], 'full', cl('foto_estatica') ) .
								obterImg( $item['foto_dinamica'], 'full', cl('foto_dinamica') )
								?>
							</div><!-- .item_imagem -->
							<div class="item_conteudo">
								<?=
								E::h4( $item['nome'], 'item_nome' ) .
								E::p( $item['cargo'], 'item_cargo' ) .
								formatarEmail( $item['email'], 'item_email' ) .
								E::a( 'tel:' . $item['telefone'], $item['telefone'], 0, 'item_telefone' )
								?>
							</div><!-- .item_conteudo -->
						</section><!-- .individuos_item -->
					</div><!-- .col -->
					<?php
					row( $n, $classeCols );
				endforeach;
				?>
			</div><!-- .row -->
		</div><!-- .executivos_individuos -->
		
	</section><!-- #executivos -->
	
	<?php
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// PARCERIAS
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	extract( $parcerias[0], EXTR_PREFIX_ALL, 'par' );
	
	?>
	<section id="parcerias" class="container">
	
		<?=
		E::h2( null, 'titulo parcerias_titulo' )
			->span( $par_titulo1, 'linha1' )
			->span( $par_titulo2, 'linha2' )
		?>
		
		<div class="row thumb-clientes margem-30">
		  <div class="col-6 col-sm-4 col-md-4 col-lg-3">
			<img src="http://via.placeholder.com/350x350" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>
		  <div class="col-6 col-sm-4 col-md-4 col-lg-3">
			<img src="http://via.placeholder.com/350x350" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-4 col-md-4 col-lg-3">
			<img src="http://via.placeholder.com/350x350" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-4 col-md-4 col-lg-3">
			<img src="http://via.placeholder.com/350x350" alt="..." class="img-fluid img-thumbnail">
		  </div>
		</div><!-- .row -->
		
	</section><!-- #parcerias -->

</div><!-- #quem-somos -->

<?php

get_footer();

