<?php
global $post, $tema, $wp_query;

if ( is_singular() || is_front_page() )
	the_post();

extract( get_fields('widget_acf_widget_3'), EXTR_PREFIX_ALL, 'cabecalho' );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php /*
	<link rel="icon" href="<?= TEMA ?>/imagens/favicon.png" sizes="32x32" />
	<link rel="icon" href="<?= TEMA ?>/imagens/favicon.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="<?= TEMA ?>/imagens/favicon.png" />
	<meta name="msapplication-TileImage" content="<?= TEMA ?>/imagens/favicon.png" />
	*/ ?>
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="dns-prefetch" href="//<?= parse_url( get_field( 'url', PAGINA_BLOG ), PHP_URL_HOST ) ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>><div id="page" class="hfeed site">
	
	<header id="masthead" class="site-header" role="banner">
		<div id="sticky_nav">
			
			<div id="cabecalho_topo">
				<div class="row">
					
					<div class="col-xs-12 col-sm-6">
						<nav id="cabecalho_breadcrumb">
							<?php
							Componente::breadcrumb( array(
								'id'				=> 'breadcrumb_nav',
								'ignorar_cats'		=> array( CAT_BLOG, CAT_EVENTOS ),
								'rotulo'			=> pll__('Você está aqui:'),
								'icone_home'		=> null,
								'titulo_busca'		=> pll__('Busca por "%s"'),
								'titulo_404'		=> pll__('Página inexistente'),
								'is_front_page'		=> true,
								'is_home'			=> true,
								'is_page'			=> true,
								'is_404'			=> true,
							) );
							?>
						</nav>
					</div><!-- .col -->
					
					<div class="col-xs-12 col-sm-6">
						<nav id="cabecalho_linguas">
							<h3 class="linguas_rotulo">idioma</h3>
							<div class="select_wrapper">
								<?php
								pll_the_languages( array(
									'dropdown'		=> 1,
									'show_names'	=> 1,
									'hide_if_empty'	=> 0,
									'show_flags'	=> 0,
								) )
								?>
							</div><!-- .select_wrapper -->
						</nav><!-- #cabecalho_linguas -->
					</div><!-- .col -->
					
				</div><!-- .row -->
			</div><!-- #cabecalho_topo -->
			
			<div id="cabecalho_principal" class="vert_ancestral">
			
				<div id="cabecalho_logo" class="vert_item">
					<a href="<?= site_url() ?>" rel="home">
						<?php img( $cabecalho_logo, 'full', cl('cabecalho_logo_img') ) ?>
					</a>
					<button id="cabecalho_toggle" type="button" data-toggle="collapse" data-target="#menu_principal .collapse" title="Menu" class="visible-xs-block"><span id="cabecalho_toggle_rotulo">Menu</button>
				</div><!-- #cabecalho_logo -->
				
				<div id="cabecalho_direita">
					
					<nav id="menu_principal" class="vert_item">
						<div class="collapse navbar-collapse">
							<?php wp_nav_menu( array( 'theme_location' => 'principal', 'menu_class' => 'menu' ) ); ?>
						</div><!-- .collapse -->
					</nav><!-- #menu_principal -->
			
					<div id="cabecalho_newsletter" class="vert_item"><?= do_shortcode( $cabecalho_newsletter ) ?></div>
				
					<div id="cabecalho_busca" class="vert_item">
						<button id="busca_botao" type="button" class="invisivel" data-toggle="collapse" data-target="#busca_form"><?= imgTema( 'cabecalho_busca.png', 'Busca' ) ?></button>
						<form id="busca_form" class="collapse anim" <?php animAttr( 'fadeInTop', 0.4, 0 ) ?> action="<?= site_url() ?>" method="GET">
							<input id="busca_input" type="search" name="s" value="<?= esc_attr( get_search_query() ) ?>" />
							<button id="busca_submit" type="submit"><?php pll_e('Ir') ?></button>
						</form><!-- #busca_form -->
					</div><!-- #cabecalho_busca -->
					
					<nav id="cabecalho_redes" class="vert_item">
						<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu_social' ) ); ?>
					</nav><!-- #cabecalho_redes -->
				
				</div><!-- #cabecalho_direita -->

			</div><!-- #cabecalho_principal -->
			
		</div><!-- #sticky_nav -->
	</header><!-- #masthead -->
	
	<div id="content" class="site-content">
		<main id="content-area">
