<?php

get_header();

$catPrincipal = categoriaPrincipal( $post->ID );
$link = get_the_permalink( $post->ID );
$numeroComentarios = get_comments_number( $post );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('blog'); ?>>

	<header class="entry-header <?php if ( get_field( 'escurecer_fundo', PAGINA_BLOG ) ) print 'escurecer' ?>" <?php fundo( umDe( get_post_thumbnail_id( PAGINA_BLOG ), FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container ancorar">
				<?php
				elementoNaoVazio( 'h1',
					subElemento( 'span', get_the_title(), cl('large') ).
					subElementoNaoVazio( 'small', enfase( get_field('subtitulo') ), cl('small') ),
				cl('entry-title') );
				?>
			</div><!-- .container -->
		</div><!-- .header-caption -->
	</header><!-- .entry-header -->
	
	<div class="container">
		<div class="entry-main">
			<div class="entry-content">
			
				<?php
				
				if ( $thumbId = get_post_thumbnail_id( $post->ID ) )
					img( $thumbId, 'medium', cl('post_thumb alignright') );
				
				elemento( 'p', 'Postado em: ' . strftime( '%d de %B de %Y', strtotime( $post->post_date ) ), cl('post_data') );
				
				the_content();
				
				// comments_template();
				
				?>
				
			</div><!-- .entry-content -->
		</div><!-- .entry-main -->
	</div><!-- .container -->
	
</article><!-- #post-## -->

<?php

get_footer();
