<?php
/*
Plugin Name: Uploaded file name sanitizer
Plugin URI: http://dev.liudas.eu/uploaded-file-name-sanitizer
Description: Replaces chars which are not in 'a-z', '0-9' and ' '(space) range.
Version: 1.0
Author: devliudaseu
Author URI: http://dev.liudas.eu/
License: GPLv2
*/

function sanitize_name_sluggify( $string ) {
	// Forces the $string to comply with allowed characters for a variable name
	$charsFrom = array( 'à', 'á', 'ã', 'â', 'é', 'è', 'ê', 'í', 'ï', 'ì', 'ò', 'ó', 'õ', 'ô', 'ú', 'ù', 'ü', 'ç', 'ñ', '-', ' ', '.', '+', '/', '\\', '\'' );
	$charsTo   = array( 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'c', 'n', '_', '_', '_', '_', '_', '_' , '_'  );
	$string = mb_strtolower( $string, 'UTF-8' );
	$string = str_replace( $charsFrom, $charsTo, $string );
	$string = preg_replace( '#^[^a-z]+|[^a-z0-9_]#', '', $string );
	$string = preg_replace( '#_{2,}#', '_', $string );
	return $string;
}

function sanitize_name_devliudaseu( $filename ) {
    $info = pathinfo( $filename );
    $ext  = empty( $info['extension'] ) ? '' : '.' . $info['extension'];
    $name = basename( $filename, $ext );
	// MODIFICADO - Utilização de função sluggify própria, incluída acima
    // $name = preg_replace( '/[^a-z0-9_ ]/i', '-', $name );
	$name = sanitize_name_sluggify( $name );
    return $name . $ext;
}

add_filter( 'sanitize_file_name', 'sanitize_name_devliudaseu', 10 );